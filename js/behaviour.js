jQuery(document).ready(function() {
    jQuery(window).scroll(function() {
        fixedElement();
    });
});


function fixedElement() {
    console.log("Scroll");
    var fixedElement = jQuery('nav');
    var windowPosition = jQuery(window).scrollTop();
    var elementPosition = jQuery("header").offset().top;

    if (windowPosition > elementPosition) {
        fixedElement.addClass("fixed");
    } else {
        fixedElement.removeClass("fixed");
    }
};